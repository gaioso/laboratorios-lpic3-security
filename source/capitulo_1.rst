Instalación de FreeIPA
======================

.. code-block:: bash

   yum search ipa
   yum search ipa | grep server

   yum install ipa-server ipa-server-dns

   ipa-server-install --setup-dns --idstart=5000 --idmas=10000

   kinit admin
   klist

   ipa user-find admin

   ipactl status
